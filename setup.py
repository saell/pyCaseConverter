from setuptools import setup, find_packages

setup(name='py_case_converter',
      version='0.1',
      description='Converts different case styles of strings',
      url='https://gitlab.com/saell/pyCaseConverter',
      author='saell',
      license='MIT',
      packages=find_packages(),
      zip_safe=False)
