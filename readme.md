
# pyCaseConverter

This project aims to provide case conversions between different case styles.

Here some "definitions" found on [Wikipedia](https://en.wikipedia.org/wiki/Letter_case#Special_case_styles):

  [CamelCase](https://en.wikipedia.org/wiki/Camel_case)
>     "TheQuickBrownFoxJumpsOverTheLazyDog"
> Spaces and punctuation are removed and the first letter of each word is capitalised. If this includes the first letter of the first word ("CamelCase", "PowerPoint", "TheQuick...", etc.), the case is sometimes called upper camel case (or, when written, "CamelCase"), Pascal case[22] or bumpy case. When, otherwise, the first letter of the first word is lowercase ("camelCase", "iPod", "eBay", etc.), the case is usually known as camelCase and sometimes as lower camel case. This is the format that has become popular in the branding of information technology products.

  [snake_case](https://en.wikipedia.org/wiki/Snake_case)
>     "the_quick_brown_fox_jumps_over_the_lazy_dog"
> Punctuation is removed and spaces are replaced by single underscores. Normally the letters share the same case (e.g. "UPPER_CASE_EMBEDDED_UNDERSCORE" or "lower_case_embedded_underscore") but the case can be mixed, as in OCaml modules.[23]. This may also be called "pothole case," especially in Python programming where this convention is often seen for variable naming. When all upper case, it may be referred to as "SCREAMING_SNAKE_CASE".[24]
