import unittest

from py_case_converter import PyCaseConverter


class TestConversion(unittest.TestCase):
    def test_normalize(self):
        self.assertEqual(["the", "quick", "brown", "fox", "jumps", "over", "the", "lazy", "dog"],
                         PyCaseConverter.normalize("TheQuickBrownFoxJumpsOverTheLazyDog"))
        #      self.assertEqual(["the", "quick", "brown", "fox", "jumps", "over", "the", "lazy", "dog"],
        #                      PyCaseConverter.normalize("The_quick_brown_fox_jumps_over_the_lazy_dog"))
        #     self.assertEqual(["screaming", "snake", "case"],
        #                     PyCaseConverter.normalize("SCREAMING_SNAKE_CASE"))
        self.assertEqual(["ab", "cdef"],
                         PyCaseConverter.normalize("ABCdef"))

        self.assertEqual(["my", "variable123"],
                         PyCaseConverter.normalize("myVariable123"))
        self.assertEqual(["my2variables"],
                         PyCaseConverter.normalize("my2Variables"))
        self.assertEqual(["the3rd", "variable", "is", "here"],
                         PyCaseConverter.normalize("The3rdVariableIsHere"))
        self.assertEqual(["12345nums", "at", "the", "start", "included", "too"],
                         PyCaseConverter.normalize("12345NumsAtTheStartIncludedToo"))

        self.assertEqual(["value"], PyCaseConverter.normalize("value"))
        self.assertEqual(["camel", "value"], PyCaseConverter.normalize("camelValue"))
        self.assertEqual(["title", "value"], PyCaseConverter.normalize("TitleValue"))
        self.assertEqual(["value"], PyCaseConverter.normalize("VALUE"))
        self.assertEqual(["eclipse", "rcp", "ext"], PyCaseConverter.normalize("eclipseRCPExt"))

    def test_snake_case(self):
        self.assertEqual("the_quick_brown_fox_jumps_over_the_lazy_dog",
                         PyCaseConverter.snake_case("TheQuickBrownFoxJumpsOverTheLazyDog"))
        self.assertEqual("the_quick_brown_fox_jumps_over_the_lazy_dog",
                         PyCaseConverter.snake_case("The_quick_brown_fox_jumps_over_the_lazy_dog"))
        self.assertEqual("screaming_snake_case", PyCaseConverter.snake_case("SCREAMING_SNAKE_CASE"))
        self.assertEqual("ab_cdef", PyCaseConverter.snake_case("ABCdef"))

        self.assertEqual("my_variable123", PyCaseConverter.snake_case("myVariable123"))
        self.assertEqual("my2variables", PyCaseConverter.snake_case("my2Variables"))
        self.assertEqual("the3rd_variable_is_here", PyCaseConverter.snake_case("The3rdVariableIsHere"))
        self.assertEqual("12345nums_at_the_start_included_too",
                         PyCaseConverter.snake_case("12345NumsAtTheStartIncludedToo"))

        self.assertEqual("value", PyCaseConverter.snake_case("value"))
        self.assertEqual("camel_value", PyCaseConverter.snake_case("camelValue"))
        self.assertEqual("title_value", PyCaseConverter.snake_case("TitleValue"))
        self.assertEqual("eclipse_rcp_ext", PyCaseConverter.snake_case("eclipseRCPExt"))
        self.assertEqual("value", PyCaseConverter.snake_case("VALUE"))

    def test_camelCase(self):
        self.assertEqual("theQuickBrownFoxJumpsOverTheLazyDog",
                         PyCaseConverter.camel_case("TheQuickBrownFoxJumpsOverTheLazyDog"))
        #  self.assertEqual("theQuickBrownFoxJumpsOverTheLazyDog",
        #                  PyCaseConverter.camel_case("The_quick_brown_fox_jumps_over_the_lazy_dog"))
        #        self.assertEqual("screamingSnakeCase", PyCaseConverter.camel_case("SCREAMING_SNAKE_CASE"))
        self.assertEqual("abCdef", PyCaseConverter.camel_case("ABCdef"))

        self.assertEqual("myVariable123", PyCaseConverter.camel_case("myVariable123"))
        self.assertEqual("my2variables", PyCaseConverter.camel_case("my2Variables"))
        self.assertEqual("the3rdVariableIsHere", PyCaseConverter.camel_case("The3rdVariableIsHere"))
        self.assertEqual("12345numsAtTheStartIncludedToo",
                         PyCaseConverter.camel_case("12345NumsAtTheStartIncludedToo"))

        self.assertEqual("value", PyCaseConverter.camel_case("value"))
        self.assertEqual("camelValue", PyCaseConverter.camel_case("camelValue"))

        self.assertEqual("eclipseRcpExt", PyCaseConverter.camel_case("eclipseRCPExt"))
        self.assertEqual("value", PyCaseConverter.camel_case("VALUE"))

    def test_PascalCase(self):
        self.assertEqual("TheQuickBrownFoxJumpsOverTheLazyDog",
                         PyCaseConverter.pascal_case("TheQuickBrownFoxJumpsOverTheLazyDog"))
        #  self.assertEqual("TheQuickBrownFoxJumpsOverTheLazyDog",
        #                  PyCaseConverter.pascal_case("The_quick_brown_fox_jumps_over_the_lazy_dog"))
        #        self.assertEqual("ScreamingSnakeCase", PyCaseConverter.pascal_case("SCREAMING_SNAKE_CASE"))
        self.assertEqual("AbCdef", PyCaseConverter.pascal_case("ABCdef"))

        self.assertEqual("MyVariable123", PyCaseConverter.pascal_case("myVariable123"))
        self.assertEqual("My2Variables", PyCaseConverter.pascal_case("my2Variables"))
        self.assertEqual("The3rdVariableIsHere", PyCaseConverter.pascal_case("The3rdVariableIsHere"))
        self.assertEqual("12345numsAtTheStartIncludedToo",
                         PyCaseConverter.pascal_case("12345NumsAtTheStartIncludedToo"))

        self.assertEqual("Value", PyCaseConverter.pascal_case("value"))
        self.assertEqual("CamelValue", PyCaseConverter.pascal_case("camelValue"))

        self.assertEqual("EclipseRcpExt", PyCaseConverter.pascal_case("eclipseRCPExt"))
        self.assertEqual("Value", PyCaseConverter.pascal_case("VALUE"))


if __name__ == '__main__':
    unittest.main()
