import re


class PyCaseConverter(object):

    @staticmethod
    def normalize(text_to_normalize):
        # taken from:
        # https://stackoverflow.com/questions/7593969/regex-to-split-camelcase-or-titlecase-advanced/7599674#7599674
        pattern = re.compile("(?<=[a-z])(?=[A-Z])<|(?<=[A-Z])(?=[A-Z][a-z])")
        splitted = re.split(pattern, text_to_normalize)

        return [x.lower() for x in splitted]

    @staticmethod
    def snake_case(text_to_convert):
        return '_'.join(PyCaseConverter.normalize(text_to_convert))

    @staticmethod
    def camel_case(text_to_convert):
        normalized = PyCaseConverter.normalize(text_to_convert)

        uppercase_list = []
        for i, word in enumerate(normalized):
            if i == 0:
                uppercase_list.append(word)
            else:
                uppercase_list.append(word.title())

        return "".join(uppercase_list)

    @staticmethod
    def pascal_case(text_to_convert):
        normalized = PyCaseConverter.normalize(text_to_convert)

        uppercase_list = []
        for i, word in enumerate(normalized):
            uppercase_list.append(word.title())

        return "".join(uppercase_list)
